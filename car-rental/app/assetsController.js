/**
 * Created by sissoko on 17/05/2016.
 * Serve static contents.
 * public/*
 * views/*
 */

exports.serveContent = function (request, response) {
    var path = request.path;
    console.log(path);
    if (!path.startsWith("/public") && !path.startsWith("/views")) {
        response.status(403).send("Access forbidden");
    } else {
        response.sendFile(__dirname + request.path);
    }
}
