/**
 * server.js
 * Author : Cheick Mahady SISSOKO
 * Date   : 2016-05-17 22:42:45
 */

var express  = require("express");
var app      = express();
var http     = require("http");

var rLoader  = require("./routesLoader");

rLoader.loadRoutes(app);

const server = http.createServer(app).listen(9000, 
	function(error) {
		if (error) {
			console.log(error);
		} else {
			console.log("Server listening on http://localhost:%s", server.address().port);
		}
	});

