/**
 * controllers.js
 * Controller User sessions and home page
 *
 * Author : Cheick Mahady SISSOKO
 * Date   : 2016/05/18 11:47:30
 */

var homeContoller = angular.module('homeController', [])
.controller('HomeCtrl', ['$scope', function($scope) {
	$scope.welcome = "Welcome in you car rental application";
}])
;
