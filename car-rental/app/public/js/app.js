/**
 * app.js
 * Author : Cheick Mahady SISSOKO
 * Date   : 2016/05/18 01:14:50
 */

var carRentApp = angular.module("carRentApp", [
	'ngRoute',
	'homeController'
])
.config(function($routeProvider){
	$routeProvider
	.when("/", {
		templateUrl: viewUrl("home"),
		controller : 'HomeCtrl'
	})		
	.when("/cars", {
		templateUrl: viewUrl("cars")
	})
	.when("/rents", {
		templateUrl: viewUrl("rents")
	})
	.when("/login", {
		templateUrl: viewUrl("login")
	})
	.otherwise({
		redirectTo: "/"
	});
});

function viewUrl(template) {
	return "/views/" + template + ".html";
} 
