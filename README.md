# NodeJS Tutorial
## Installation de NodeJS
-------------------------

Sur Linux, tapez simplement la commande suivante dans la console :  

`sudo apt install nodejs-legacy` 

Cette commande va installer la dernière version de NodeJS sur votre système.

Pour s'assurer du bon déroulement de l'installation tapez la commande suivante dans la console : 

`node --version` 

Cela donne la sortie : 

`v4.2.6` 

Ce qui montre que node s'est très bien installé.

Jouons un peu avec `node`

- Créer un fichier dans le repertoire courant par exemple : 

`vim hello.js` 

Modifier ce fichier avec le texte suivant : 
```
console.log("Hello world!");
```
Sauvegarder le fichier et executer notre code avec node en executant la commande : 

`node hello.js`

On obtient la sortie : 

`Hello world!`

Nous venons de réaliser notre premier programme JavaScript qui affiche à l'écran : Hello world!

NodeJS est un outil très puissant pour executer les JavaScripts cest ça fonction primaire :)


## Installation de NPM (Node Package Manager)
---------------------------------------------

Comme son nom l'indique `npm` est un outil qui va nous permettre de gérer les packages de nodejs.
Pour l'installer executer la commande suivante : 
`sudo apt install npm`

Comme pour node, on peut vérifier l'installatio par la commande : 
`npm --version`
On obtient la version de npm installée : 
`3.5.2`

Nous avons maintenant npm. C'est lui qui va nous permettre d'installer tous les autres packages pour réaliser notre application.

Nous allons pouvoir réaliser notre application maintenant.

# Application de location de voiture
------------------------------------
Dans ce tutoriel nous allons réaliser une application de location de voiture.

Ce tutoriel suppose avec que vous avez déjà une notion de la programmation web avec le design pattern (MVC)

Nous allons utiler les frameworks suivants : 

- Express pour le backend
- AngularJS pour le frontend
- MongoDB pour la base de données

## Création du projet
---------------------
Créer un dossier avec le nom : car-rental

`mkdir car-rental`

`cd car-rental`

Nous allons utiliser la structure des fichiers suivante pour origaniser nos fichiers.

```
car-rental/
├── app 
├── bower.json
└── package.json
```

Tous nos codes seront placés dans le dossier app/ 

Nous voyons deux fichiers avec extension .json regardons ce qui se trouve dans ces fichiers.

package.json

```
{
  "version": "0.0.1",
  "private": true,
  "name": "car-rental",
  "description": "Car Rental",
  "repository": "https://bitbucket.com/csissoko/nodejs.git",
  "license": "MIT",
  "devDependencies": {
    "bower": "^1.3.1",
    "shelljs": "^0.2.6",
    "mongoose": "4.4.16",
    "bcryptjs": "latest"
  },
  "scripts": {
    "postinstall": "bower install",
    "prestart": "npm install",
    "start": "npm run car-rental",
    "car-rental": "node app/server.js"
  }
}
```

bower.json

```
{
  "name": "car-rental",
  "description": "Car Rental",
  "version": "0.0.1",
  "homepage": "https://bitbucket.com/csissoko/nodejs.git",
  "license": "MIT",
  "private": true,
  "dependencies": {
    "angular": "1.5.x",
    "angular-mocks": "1.5.x",
    "jquery": "~2.1.1",
    "bootstrap": "~3.3.6",
    "angular-route": "1.5.x",
    "angular-resource": "1.5.x",
    "angular-animate": "1.5.x"
  }
}
```

On remarque une definition des dépendences et des scripts.

Toutes les dépendences déclarées dans 'devDependencies' seront téléchargées et placées dans un dossier qui s'appelle 'node\_modules/' dans un notre projet 

Les scripts sont utilisés avec `npm`

Pour installer les dépendences on execute la commande : 

`npm install`

Cette commande installe les dépendences déclarées dans le fichier package.json et celles déclarées dans bower.json mais celles-ci seront placées dans bower\_components/


Maintenant qu'on a installé nos dépendences on peut créer notre serveur.

## server.js
------------

```
/**
 * server.js
 * Author : Cheick Mahady SISSOKO
 * Date   : 2016-05-17 22:42:45
 */

var express  = require("express");
var app      = express();
var http     = require("http");


const server = http.createServer(app).listen(9000,
        function(error) {
                if (error) {
                        console.log(error);
                } else {
                        console.log("Server listening on http://localhost:%s", server.address().port);
                }
        });

app.get("/", function(request, response) {
        response.send("Your Car Rental Application is ready");
});

```

Lançons notre application

`npm start`

Oops !

Une erreur, node se plaind de ne pas pouvoir reussir à charger la dépendence `express`

C'est normal on l'a pas installé. Ce n'est pas grave on va l'installer avec notre `npm` d'ailleurs c'est pourquoi on l'a installé.

`sudo npm install -g express`

Le `-g` permet d'installer express dans le depos global de node. Sans celui-ci il allait être installé dans le dossier node\_modules/

Reessayons de lancer notre application à nouveau:

`npm start`

Oops encore !!! Ça ne marche pas :(

Ne vous inquietez pas c'est normal ! On n'a pas dit à node où il doit aller chercher les composantes installées. Pour cela nous allons definir la variable d'environnement `NODE_PATH`

Pour cela ajoutons la ligne suivante dans notre `~/.profile`

`vim ~/.profile`

```
...
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

export NODE_PATH=/usr/local/lib/node_modules
...
```

Réesseyons à nouveau :

`npm start`

Server listening on http://localhost:9000

Yes !

Si on ouvre un navigateur avec l'adresse http://localhost:9000

On verra notre texte : 'Your Car Rental Application is ready'

Voilà on créé un serveur http :)

### Explications :

On recupère une instance de express qu'on garde dans la variable app
On crée ensuite notre serveur avec la méthode `createServer` de http qui prend notre app en parametre.

On écoute ensuite sur le port 9000.

Après on associe la route / à une action avec la méthode `app.get()`

# Réorganisation
----------------

## routes
---------
Nous allons utiliser le fichier routes pour déclarer nos différentes routes.
Cela nous permettra de centraliser toutes nos routes au même endroit et ajouter facilement de nouvelles routes.
Et pouvoir regrouper les actions de même nature dans un fichier séparé.

routes
```
# Method 	Route		Action (controllerName.Method)
GET		/		home.index

# Assets
GET             /public/*       assets.serveContent
GET             /views/*        assets.serveContent
```

## Routes Loader
----------------
Cela permet de charger toutes les routes du fichier `routes` et l'ajouter dans notre application.

routesLoader.js
```
/**
 * Created by sissoko on 12/05/2016.
 */
var controllers = {
    home: require("./homeController"),
    assets: require("./assetsController")
}

var Route = function (routeLine) {
    this.method = routeLine[0];
    this.route = routeLine[1];
    var actionParts = routeLine[2].split(".");
    var controller = actionParts[0];
    var action = actionParts[1];
    this.action = controllers[controller][action];
}

exports.loadRoutes = function (app) {
    var fs = require('fs');
    var path = require('path');
    var filePath = path.join(__dirname, "routes");
    var lineReader = require('readline').createInterface({
        input: fs.createReadStream(filePath)
    });
    lineReader.on('line', function (line) {
        var route = lineParser(line);
        if (route) {
            addRoute(route, app);
        }
    });
};

var lineParser = function (line) {
    var reg = new RegExp("[ \t]+", "g");
    var lineCount = 0;
    line = line.trim();
    lineCount++;
    if (line != '' && !line.startsWith("#")) {
        var routeLine = line.split(reg);
        if (routeLine.length != 3) {
            throw "Error in line[" + lineCount + "]\n" + line
        }
        var route = new Route(routeLine);
        return route;
    } else {
        if (line.startsWith("#")) {
            console.log("Commented line " + line)
        }
    }
}

var addRoute = function (route, app) {
    if (route.action !== undefined) {
        if (route.method === 'GET') {
            app.get(route.route, route.action);
        } else if (route.method === 'POST') {
            app.post(route.route, route.action);
        }
    } else {
        throw "Route [" + line + "] not implemented";
    }
}
```

On modifie ensuite le fichier server.js pour ressembler à celui-ci :
```
/**
 * server.js
 * Author : Cheick Mahady SISSOKO
 * Date   : 2016-05-17 22:42:45
 */

var express  = require("express");
var app      = express();
var http     = require("http");

var rLoader  = require("./routesLoader");

rLoader.loadRoutes(app);

const server = http.createServer(app).listen(9000,
        function(error) {
                if (error) {
                        console.log(error);
                } else {
                        console.log("Server listening on http://localhost:%s", server.address().port);
                }
        });
```

On deplace la méthode avec `app.get("/", function(...){...})` dans le fichier `homeController.js`

Nous pouvons nous concentrer sur le développement de notre application après avoir compris la structure du projet.

# HTML
------
Nous allons modifier notre controller afin qu'il envoie un fichier à la place du texte.
Pour cela creons un fichier index.html dans le dossier views/

`vim views/index.html`

```
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Car Rental Application</title>
</head>
<body>
	<h1>Your Car Rental Application is ready</h1>
</body>
</html>
```

Nous venons d'associer notre route / au fichier index.html.

Testons notre application

`npm start`

# Bootstrap & jQuery
--------------------
Bootstrap nous permet d'avoir des pages plus jolies. Nous allons donc l'importer dans notre page index.html

pour cela modifier le fichier views/index.html

`vim views/index.html`

views/index.html
```
...
<head>
    <meta charset="utf-8">
    <title>Car Rental Application</title>
    <link rel="stylesheet" href="/public/bower_components/bootstrap/dist/css/bootstrap.css">

    <script src="/public/bower_components/jquery/dist/jquery.js"></script>
</head>
...
```

Ajoutons quelques menus à notre application : 

- Home
- Cars
- Rents
- My Profile / Login
	- My Rents
	- Logout

Cela donne : 
```
<nav id="navbar" class="navbar navbar-default">
   <div class="container-fluid">
   <!-- Brand and toggle get grouped for better mobile display -->
   	<div class="navbar-header">
  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-menus"
                    aria-expanded="false">
 	 <span class="sr-only">Toggle navigation</span>
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
   <span class="icon-bar"></span>
   </button>
   <a class="navbar-brand" href="#/">Car Rental</a>
   </div>
   <!-- Collect the nav links, forms, and other content for toggling -->
   <div class="collapse navbar-collapse" id="header-menus">
   <ul class="nav navbar-nav">
   </ul>
   <ul class="nav navbar-nav navbar-right">
   <li class="active">
   <a href="#/">
              Home
                    </a>
   </li>
   <li>
   <a href="#/cars">Cars</a>
   </li>
   <li>
   <a href="#/rents">Rents</a>
   </li>
   <li>
   <a href="#/login">Login</a>
   </li>
   </ul>
   </div> <!-- /.navbar-collapse -->
   </div> <!-- /.container-fluid -->
</nav>
```
# AngularJS
-----------
Importons maintenant les fichiers d'angularJS dans notre index.html

```
...
<script src="/public/bower_components/jquery/dist/jquery.js"></script>
<script src="/public/bower_components/angular/angular.js"></script>
<script src="/public/bower_components/angular-animate/angular-animate.js"></script>
<script src="/public/bower_components/angular-route/angular-route.js"></script>
<script src="/public/bower_components/angular-resource/angular-resource.js"></script>
...
```
Dans le dossier app/public/
creons un dossier js/ qui contient tous les fichiers js d'angular

`mkdir public/js`

Ajoutons un fichier app.js dans ce dossier qui va charger le module angular de notre projet.

`vim app.js`

```
var carRentApp = angular.module("carRentApp", [
        'ngRoute'
])
.config(function($routeProvider) {

});
```
On commence par créer le module avec le nom `carRentApp`
Ensuite on configure avec nos différentes routes gérées pas angular. `ngRoute` est le module d'angular qui gère le routage.

Chargeons maintenant notre module angular dans notre fichier index.html. On n'avait juste chargé les fichiers d'angular mais dans cette étape nous allons chargé le module dans nos views.

```
<!doctype html>
<html lang="en" ng-app="carRentApp">
...
<script src="/public/js/app.js"></script>
<head>
...
</html>
```

Pour charger les vues dynamiquement avec angular, il nous faut déclarer un ng-view dans notre index.html

```
...
<div ng-view></div>
...
```

Créons un fichier qui sera placé dans notre ng-view lorsqu'on sera sur la route /.

views/home.html
```
<h1>Your Car Rental Application is ready</h1>
```

Ajoutons la route à angular pour qu'il charge notre home.html lorsqu'on sera sur la route /.

```
        $routeProvider
	.when("/", {
                templateUrl: "/views/home.html"
        })
	.otherwise({
		redirectTo: "/"
	});
```

On dit au gestionnaire des routes de remplacer le ng-view quand la route est / et rediriger toute autre route vers /.

Maintenant qu'on sait créer des routes ajoutons les autres routes qu'on a dans notre menu de navigation à savoir :

- Cars : '/cars'
- Rents: '/rents'
- Login: '/login'

```
...
.config(function($routeProvider){
        $routeProvider
        .when("/", {
                templateUrl: viewUrl("home")
        })
        .when("/cars", {
                templateUrl: viewUrl("cars")
        })
        .when("/rents", {
                templateUrl: viewUrl("rents")
        })
        .when("/login", {
                templateUrl: viewUrl("login")
        })
        .otherwise({
                redirectTo: "/"
        });
});

function viewUrl(template) {
        return "/views/" + template + ".html";
}
```

Vous remarquez une nouvelle fonction `viewUrl(template)`. L'avantage de cette fonction est de simplifier la modification de la racine des vues.

Si on décide que les .html ne seront plus dans views/, il suffit de modifier cette fonction.

Quand on rafraichi notre nagivateur et si on clique sur un menu on voit que l'url change mais la page ne change pas. C'est normal car on n'a pas créé les html qu'on a donné en paramètre de `templateUrl`

On va les créer maintenant dans notre dossier views/

`vim views/cars.html` 

```
<h1>Cars' Page</h1>
<p> We'll put cars content on this page</p>
```

Quand clique sur le menu `Cars` on voit bien notre contenu.

Je n'aime pas trop que les textes collent au bord du navigateur. Heureusement que bootstrap offre des classes css qui me permet de modifier cela.

Modifions notre fichier index.html afin d'ajouter la classe `container` autour de notre `ng-view`

`vim view/index.html`
```
...
<div class="container">
    <div ng-view></div>
</div>
...
```

Faisons pareil pour les autres html.

`vim views/rents.html`
```
<h1>Rents Page</h1>
<p>We'll put rents content on this page</p>
```

`vim views/login.html`
```
<h1>Login's Page</h1>
<p>We'll put login content on this page</p>
```

On va rentrert maintenant dans le vif de angular

# Controllers Angular
---------------------

C'est dans les controllers que nous allons gérer les données qu'il faut afficher dans les views.

Créons un fichier controllers.js dans le dossier js et importons le dans notre index.html

`vim js/controllers.js`

```
var homeContoller = angular.module('homeController', [])
.controller('HomeCtrl', ['$scope', function($scope) {
        $scope.welcome = "Welcome in you car rental application";
}])
;
```
On a créé un module qui s'appelle `homeController`
Le paramètre `[]` signifie que notre module ne depend pas d'autre module à part les modules de base qu'offre angular qu'on n'a pas besoin de déclarer explicitement la dépendence.

On crée ensuite un controller avec le nom `HomeCtrl` par la méthode `.controller(controllerName, ['deps', function(deps) {...})`
Le paramètre `$scope` est le module qui nous permet d'avoir accès à nos variables dans la vue. Par exemple la variable `welcome` sera accessible dans la view qui aura `HomeCtrl` comme controller ; dans notre cas ça sera `index.html`.

## Comment associer un controller à une view ?

Cette association se fait lors du mapping des routes on va donc modifier le fichier `app.js` pour associer le controller `HomeCtrl` à `index.html`

```
	.when("/", {
                templateUrl: viewUrl("home"),
                controller : 'HomeCtrl'
        })
```
Modifions le fichier views/home.html pour afficher notre message `welcome`

views/home.html
```
<h1>Your Car Rental Application is ready</h1>
<h2>{{welcome}}</h2>
```

Si on actualise notre page on ne verra pas le message mais `{{welcome}}` c'est tout ce qui a de normal, car on n'a pas déclarer la dépendence dans notre module dans le fichier `app.js`

Modifier le fichier `js/app.js` comme suit : 

```
var carRentApp = angular.module("carRentApp", [
        'ngRoute',
        'homeController'
])
...
```

Maintenant si on actualise on verra notre message de bienvenu :)


